import apiMobil from '../../Apis/ApisMobil'
import { ActionTypes } from "../contants/action-types"

export const fetchDatas = () => async (dispatch) => {
    const response = await apiMobil.get('/cars');
    dispatch({ type: ActionTypes.FETCH_DATAS, payload: response.data })
};

export const fetchData = (id) => async (dispatch) => {
    const response = await apiMobil.get(`/cars/${id}`);
    dispatch({ type: ActionTypes.FETCH_DATA, payload: response.data })
};

export const removeSelectedData = () => {
    return {
        type: ActionTypes.REMOVE_SELECTED_DATA,
    };
};

export const setButton = (btn) => {
    return {
        type: ActionTypes.SET_BUTTON,
        payload: btn,
    }
}

export const setSearch = (cars) => {
    return {
        type: ActionTypes.SEARCH_DATAS,
        payload: cars,
    }
}