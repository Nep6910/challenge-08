import { combineReducers } from 'redux';
import { buttonReducer, dataReducer, searchDatasReducer, selectedDataReducer, userReducer } from './dataReducer';

const reducers = combineReducers({
    allDatas: dataReducer,
    data: selectedDataReducer,
    btn: buttonReducer,
    filter: searchDatasReducer
});

export default reducers;