import { ActionTypes } from "../contants/action-types";

const initialState = {
    datas: [],
    button: '',
    searchDatas: [],
    user: []
}

export const dataReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.FETCH_DATAS:
            return { ...state, datas: payload };
        default:
            return state;
    }
}

export const selectedDataReducer = (state = {}, { type, payload }) => {
    switch (type) {
        case ActionTypes.FETCH_DATA:
            return { ...state, ...payload }
        case ActionTypes.REMOVE_SELECTED_DATA:
            return {}
        default:
            return state;
    }
};

export const buttonReducer = (state = initialState.button, { type, payload }) => {
    switch (type) {
        case ActionTypes.SET_BUTTON:
            return { ...state, button: payload };
        default:
            return state;
    }
}

export const searchDatasReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case ActionTypes.SEARCH_DATAS:
            return { ...state, searchDatas: payload };
        default:
            return state;
    }
}