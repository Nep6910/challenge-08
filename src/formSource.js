export const carInputs = [
    {
      id: "name",
      label: "Name",
      type: "text",
      placeholder: "Name",
    },
    {
      id: "category",
      label: "Category",
      type: "text",
      placeholder: "Category",
    },
    {
      id: "price",
      label: "Price",
      type: "number",
      placeholder: "Price",
    },
    {
      id: "passengers",
      label: "Passengers",
      type: "number",
      placeholder: "Passengers",
    },
    {
      id: "transmission",
      label: "Transmission",
      type: "text",
      placeholder: "Transmission",
    },
    {
      id: "caryear",
      label: "Car Year",
      type: "number",
      placeholder: "Car Year",
    },
    {
      id: "startrent",
      label: "Start Rent",
      type: "date",
    },
    {
      id: "finishrent",
      label: "Finish Rent",
      type: "date",
    },
  ];