import {Grid } from '@mui/material'
import React from 'react'
import { FiFacebook, FiInstagram, FiTwitter, FiTwitch} from "react-icons/fi";
import { HiOutlineMail } from "react-icons/hi";

function Footer() {

    return (
        <div className='Footer'>
            <Grid container sx={{padding: '1rem'}}>
                <Grid item sm={3}>
                    <div className="kiri">
                        <p style={{paddingRight:'4rem'}}>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>      
                </Grid>
                <Grid item sm={3} style={{justifyContent: 'center', display: 'flex'}}>
                    <div className="tengahkiri">
                        <p>Our services</p>
                        <p>Why Us</p>
                        <p>Testimonial</p>
                        <p>FAQ</p>
                    </div>
                </Grid>
                <Grid item sm={3}>
                    <div className="tengahkanan">
                        <p>Connect with us</p>
                        <div className="menuicon">
                            <div className="icon">
                                <FiFacebook size={20}/>
                            </div>
                            <div className="icon">
                                <FiInstagram size={20}/>
                            </div>
                            <div className="icon">
                                <FiTwitter size={20}/>
                            </div>
                            <div className="icon">
                                <HiOutlineMail size={20}/>
                            </div>
                            <div className="icon">
                                <FiTwitch size={20}/>
                            </div>
                        </div>         
                    </div>
                </Grid>
                <Grid item sm={3}>
                    <div className="kanan">
                        <p>Copyright Binar 2022</p>
                        <div className="Logo"></div>
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default Footer