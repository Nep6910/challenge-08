import React, { useState } from 'react'
import { Box, Grid, FormControl, Select, MenuItem, Typography, Button, Paper } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux';
import { setSearch } from '../Redux/actions/dataActions';
import { Link } from 'react-router-dom';

function FilterSearch() {
    const dispatch = useDispatch()

    const datas = useSelector((state) => state.allDatas.datas)
    const tahun = [...new Set(datas.map(datas => datas.time))].sort()
    const kategori = [...new Set(datas.map(datas => datas.category))].sort()
    const kapasitas = [...new Set(datas.map(datas => datas.passengers))].sort()

    const [mobil, setMobil] = useState('')
    const [carsCategory, setCarsCategory] = useState('')
    const [year, setYear] = useState('')
    const [passengers, setPassengers] = useState('')

    const handleSearch = () => {
        const newData = datas
            .filter(datas => datas.name === (mobil === '' ? datas.name : mobil))
            .filter(datas => datas.category === (carsCategory === '' ? datas.category : carsCategory))
            .filter(datas => datas.time === (year === '' ? datas.time : year))
            .filter(datas => datas.passengers === (passengers === '' ? datas.passengers : Number(passengers)))
        dispatch(setSearch(newData))
    }

    return (
        <>
            <Box className='FilterSearch' sx={{ display: 'flex', justifyContent: 'center', transform: 'translateY(-50%)' }}>
                <Paper elevation={5} sx={{ p: 4, width: '90%' }} >
                    <Grid container>
                        <Grid item xs={2.5}>
                            <Box>
                                <Typography>Nama Mobil</Typography>
                                <Box sx={{ width: '80%' }}>
                                    <FormControl fullWidth>
                                        <Select
                                            displayEmpty
                                            onChange={(e) => setMobil(e.target.value)}
                                            inputProps={{ 'aria-label': 'Without label' }}
                                            size='small'
                                        >
                                            <MenuItem value=''><em>Pilih Mobil</em></MenuItem>
                                            {
                                                datas.map((item) => {
                                                    return (
                                                        <MenuItem value={item.name}>
                                                            {item.name}
                                                        </MenuItem>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Box>
                        </Grid>

                        <Grid item xs={2.5}>
                            <Box>
                                <Typography>Category</Typography>
                                <Box sx={{ width: '80%' }}>
                                    <FormControl fullWidth>
                                        <Select
                                            displayEmpty
                                            onChange={(e) => setCarsCategory(e.target.value)}
                                            inputProps={{ 'aria-label': 'Without label' }}
                                            size='small'
                                        >
                                            <MenuItem value=''><em>Pilih Category</em></MenuItem>
                                            {
                                                kategori.map((item) => {
                                                    return (
                                                        <MenuItem value={item}>
                                                            {item}
                                                        </MenuItem>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Box>
                        </Grid>

                        <Grid item xs={2.5}>
                            <Box>
                                <Typography>Tahun</Typography>
                                <Box sx={{ width: '80%' }}>
                                    <FormControl fullWidth>
                                        <Select
                                            displayEmpty
                                            onChange={(e) => setYear(e.target.value)}
                                            inputProps={{ 'aria-label': 'Without label' }}
                                            size='small'
                                        >
                                            <MenuItem value=''><em>Pilih Tahun</em></MenuItem>
                                            {
                                                tahun.map((item) => {
                                                    return (
                                                        <MenuItem value={item}>
                                                            {item}
                                                        </MenuItem>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Box>

                        </Grid>
                        <Grid item xs={3}>
                            <Box>
                                <Typography>Penumpang (optional)</Typography>
                                <Box sx={{ width: '80%' }} >
                                    <FormControl fullWidth>
                                        <Select
                                            displayEmpty
                                            onChange={(e) => setPassengers(e.target.value)}
                                            inputProps={{ 'aria-label': 'Without label' }}
                                            size='small'
                                        >
                                            <MenuItem value=''><em>Pilih Penumpang</em></MenuItem>
                                            {
                                                kapasitas.map((item) => {
                                                    return (
                                                        <MenuItem value={item}>
                                                            {item}
                                                        </MenuItem>
                                                    )
                                                })
                                            }
                                        </Select>
                                    </FormControl>
                                </Box>
                            </Box>
                        </Grid>
                        <Grid item xs={1.5} sx={{ my: 'auto', mb: '4px' }}>
                            <Link to={'/home/search/'}>
                                <Button
                                    onClick={() => handleSearch()}
                                    variant='contained'
                                    style={{ color: 'white', backgroundColor: '#5CB85F', fontWeight: 'bold' }}>
                                    Cari Mobil
                                </Button>
                            </Link>
                        </Grid>
                    </Grid>
                </Paper>
            </Box>
        </>
    )
}

export default FilterSearch