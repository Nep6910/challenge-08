import React, { useContext } from 'react'
import { AppBar, Button, Toolbar, Box, Avatar } from '@mui/material'
import { Link } from 'react-router-dom'
import { AuthContext } from '../context/AuthContext'

function Navbar({ user }) {

    const { dispatch } = useContext(AuthContext)

    const logout = (e) => {
        e.preventDefault()

        dispatch({ type: 'LOGOUT' })
    }

    return (
        <>
            <Box className='Navbar'>
                <AppBar style={{ background: '#F1F3FF', color: 'black' }} elevation={0} position='static'>
                    <Toolbar style={{ padding: '0', justifyContent: 'space-between' }}>

                        <Link to='/home'><div className="Logo"></div></Link>

                        <Box className="menu">
                            <p>Our Services</p>
                            <p>Why US</p>
                            <p>Testimonial</p>
                            <p>FAQ</p>
                            {/* <p>{user.displayName}</p> */}
                            {/* <Avatar alt="Users" src={user.photos[0].value} /> */}
                            <Button
                                variant='contained'
                                style={{ color: 'white', backgroundColor: '#5CB85F', fontWeight: 'bold' }}
                                onClick={logout}
                            >
                                Logout
                            </Button>
                        </Box>
                    </Toolbar>
                </AppBar>
            </Box>
        </>
    )
}

export default Navbar