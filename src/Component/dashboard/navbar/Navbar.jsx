import './navbar.scss'
import { FiMenu, FiChevronDown, FiSearch } from "react-icons/fi";
import { useContext } from 'react';
import { AuthContext } from '../../../context/AuthContext';
import { Link } from 'react-router-dom';

const Navbar = () => {
  const { dispatch } = useContext(AuthContext)

  const handleLogout = (e) => {
    e.preventDefault()

    dispatch({ type: 'LOGOUT' })
  }

  return (
    <div className="navbar">
      <div className="wrapper">
        <div className="left">
          <div className="item">
            <img className="logo" src="/img/logo.png" alt="Logo Binar 2" />
          </div>
          <div className="item">
            <FiMenu className='icon' />
          </div>
        </div>
        <div className="right">
          <div className="item">
            <Link to='/home'>
              <button className='clientbutton'>CLIENT</button>
            </Link>
          </div>
          <div className="item">
            <div className="search">
              <FiSearch className='icon' />
              <input type="text" placeholder='Search...' />
            </div>
            <button className="searchButton">Search</button>
          </div>
          <div className="item">
            <img
              src="https://secure.gravatar.com/avatar/751c200fb69c4a196bd44b048bb2f620?s=800&d=identicon"
              alt=""
              className="avatar"
            />
            <span>User</span>
            <FiChevronDown className='icon' color='black' />
          </div>
          <div className="item">
            <button className='logoutbutton' onClick={handleLogout}>LOGOUT</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar