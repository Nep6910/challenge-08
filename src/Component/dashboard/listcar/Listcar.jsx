import { CircularProgress } from '@mui/material';
import { collection, doc, deleteDoc, onSnapshot } from 'firebase/firestore';
import { useEffect, useState } from 'react';
import { FiClock, FiEdit, FiKey, FiTrash2, FiUsers, FiSettings, FiCalendar } from 'react-icons/fi'
import { Link } from 'react-router-dom';
import { db } from '../../../firebase';
import './listcar.scss'

const Listcar = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        //LISTEN (REALTIME)
        const unsub = onSnapshot(collection(db, "cars"), (snapShot) => {
            let list = [];
            snapShot.docs.forEach(doc => {
                list.push({ id: doc.id, ...doc.data() });
            });
            setData(list)
        }, (error) => {
            console.log(error);
        });

        return () => {
            unsub();
        }
    }, [])

    const handleDelete = async (id) => {
        try {
            await deleteDoc(doc(db, "cars", id));
            setData(data.filter((item) => item.id !== id))
        } catch (err) {
            console.log(err);
        }
    }

    const listCar = data.map((datacar) => {
        const currency = parseFloat(datacar.price)

        return (
            <div className="card" key={datacar.id}>
                <div className="top">
                    <img src={datacar.img} alt="gambarmobil" />
                </div>
                <div className="center">
                    <div className="title">
                        <p>{datacar.name} / {datacar.category}</p>
                    </div>
                    <div className="price">
                        <h3>Rp. {currency.toLocaleString('id-ID')} / Hari</h3>
                    </div>
                    <div className="item">
                        <FiUsers className='icon' />
                        <p>{datacar.passengers} Person</p>
                        
                        <div className="itemplus">
                            <FiSettings className='icon' />
                            <p>{datacar.transmission}</p>
                        </div>
                        <div className="itemplus">
                            <FiCalendar className='icon' />
                            <p>Year {datacar.caryear}</p>
                        </div>
                    </div>
                    <div className="item">
                        <FiKey className='icon' />
                        <p>{datacar.startrent} - {datacar.finishrent}</p>
                    </div>
                    <div className="item">
                        <FiClock className='icon' />
                        <p>Updated at {datacar.updatedat}</p>
                    </div>

                </div>
                <div className="bottom">
                    <button className='deleteButton' onClick={() => handleDelete(datacar.id)}>
                        <FiTrash2 className='icon' />
                        Delete
                    </button>
                    <Link to={`/dashboard/cars/edit/${datacar.id}`} className='editButton'>
                        <FiEdit className='icon' />
                        Edit
                    </Link>
                </div>
            </div>
        )
    })

    if (data < 1) {
        return (
            <div className='listcarload'>
                <CircularProgress />
            </div>
        )
    } else {
        return (
            <div className="listcar">
                {listCar}
            </div>
        )
    }
}

export default Listcar