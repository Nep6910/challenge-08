import './sidebar2.scss'
import { Link } from 'react-router-dom';

const Sidebar2 = ({ page, title }) => {
    return (
        <div className="sidebar2">
            <div className="top">
                <ul>
                    <li>
                        <span>{page}</span>
                    </li>
                </ul>
            </div>
            <div className="center">
                <ul>
                    <Link to={page === "DASHBOARD" ? "/dashboard/" : "/dashboard/cars"} style={{ textDecoration: "none" }}>
                        <li>
                            <span>{title}</span>
                        </li>
                    </Link>
                </ul>
            </div>
        </div>
    )
}

export default Sidebar2