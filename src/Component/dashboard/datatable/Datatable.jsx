import './datatable.scss'
import { DataGrid } from '@mui/x-data-grid';
import { useEffect, useState } from 'react';
import { collection, onSnapshot } from "firebase/firestore";
import { db } from '../../../firebase'
import { CircularProgress } from '@mui/material';

const userColumns = [

    { field: "id", headerName: "ID", width: 100 },
    {
        field: "name",
        headerName: "Name",
        width: 250,
        renderCell: (params) => {
            return (
                <div className="cellWithImg">
                    <img className="cellImg" src={params.row.img} alt="avatar" />
                    {params.row.name}
                </div>
            );
        },
    },
    {
        field: "category",
        headerName: "Category",
        width: 100,
    },
    {
        field: "price",
        headerName: "Price",
        width: 150,
        renderCell: (params) => {
            const currency = parseFloat(params.row.price)
            return (
                <div className={`cellWithStatus ${currency.toLocaleString('id-ID')}`}>
                    Rp. {currency.toLocaleString('id-ID')}
                </div>
            );
        },
    },
    {
        field: "passengers",
        headerName: "Passengers",
        width: 110,
    },
    {
        field: "transmission",
        headerName: "Transmission",
        width: 120,
    },
    {
        field: "caryear",
        headerName: "Year",
        width: 100,
    },
    {
        field: "startrent",
        headerName: "Start Rent",
        width: 100,
    },
    {
        field: "finishrent",
        headerName: "Finish Rent",
        width: 100,
    },
    {
        field: "createdat",
        headerName: "Created At",
        width: 160,
    },
    {
        field: "updatedat",
        headerName: "Updated At",
        width: 160,
    },
];

const Datatable = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        //LISTEN (REALTIME)
        const unsub = onSnapshot(collection(db, "cars"), (snapShot) => {
            let list = [];
            snapShot.docs.forEach(doc => {
                list.push({ id: doc.id, ...doc.data() });
            });
            setData(list)
        }, (error) => {
            console.log(error);
        });

        return () => {
            unsub();
        }
    }, [])

    if (data < 1) {
        return(
            <div className='datatableload'>
                <CircularProgress />
            </div>
        )
    } else {
        return (
            <div className='datatable'>
                <DataGrid
                    rows={data}
                    columns={userColumns}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    checkboxSelection
                />
            </div>
        )
    }
}

export default Datatable