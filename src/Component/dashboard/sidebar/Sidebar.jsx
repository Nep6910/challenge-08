import './sidebar.scss'
import { FiHome, FiTruck } from "react-icons/fi";
import { Link } from 'react-router-dom';

const Sidebar = ({page}) => {
    return (
        <div className="sidebar">
            <div className="top">
                <Link to='/dashboard/' style={{ textDecoration: 'none' }}>
                    <img className="logo" src='/img/logo1.png' alt='Logo Binar'/>
                </Link>
            </div>
            <div className="center">
                <ul>
                    <Link to='/dashboard/' style={{ textDecoration: 'none' }}>
                        <li className={page === "Dashboard" ? 'liDashboard' : 'liCars'}>
                            <FiHome className="icon" />
                            <span>Dashboard</span>
                        </li>
                    </Link>
                    <Link to="/dashboard/cars" style={{ textDecoration: "none" }}>
                        <li className={page === "Cars" ? 'liDashboard' : 'liCars'}>
                            <FiTruck className="icon" />
                            <span>Cars</span>
                        </li>
                    </Link>
                </ul>
            </div>
        </div>
    )
}

export default Sidebar