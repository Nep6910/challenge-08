import React from 'react'
import { Box, Typography, Grid } from '@mui/material'

function Hero() {

    return (
        <>
            <Box
                className='Hero'
                sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between'
                }}
            >
                <Grid container>
                <Grid item sm={6} style={{display:'flex', alignItems:'center'}}>
                    <Box>
                        <Typography variant='h3' sx={{ fontWeight: 'bold'}}>
                            Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
                        </Typography>
                        <Typography variant='body1' sx={{ pr: '18rem', mt: '2rem', fontSize: 18}}>
                            Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.
                        </Typography>  
                    </Box>
                         
                </Grid>
                <Grid item sm={6}>
                    <img src='./img/gambarmobil.png' alt='' className='gambarmobil'/>
                </Grid>
            </Grid>
            </Box>
        </>
    )
}

export default Hero