import { render, screen } from "@testing-library/react"
import { BrowserRouter } from 'react-router-dom';
import '@testing-library/jest-dom';
import SignIn from "./SignIn";

test('ini string welcome admin bcr', () => {
    render(<SignIn />, {wrapper: BrowserRouter});
    const linkElement = screen.getByText(/welcome, admin bcr/i);
    expect(linkElement).toBeInTheDocument();
});

test('ini input type username', () => {
    render(<SignIn />, {wrapper: BrowserRouter});
    const username = screen.getByRole('inputUser');
    expect(username).toBeInTheDocument();
});

test('ini input type password', () => {
    render(<SignIn />, {wrapper: BrowserRouter});
    const password = screen.getByRole('inputPass');
    expect(password).toBeInTheDocument();
});

test('ini button biru', () => {
    render (<SignIn />, {wrapper: BrowserRouter});
    const buttonEl = screen.getByRole('buttonsignin');
    expect(buttonEl).toBeInTheDocument();
})

test('ini button google', () => {
    render (<SignIn />, {wrapper: BrowserRouter});
    const buttonEl = screen.getByRole('buttongoogle');
    expect(buttonEl).toBeInTheDocument();
})

test('ini button github', () => {
    render (<SignIn />, {wrapper: BrowserRouter});
    const buttonEl = screen.getByRole('buttongithub');
    expect(buttonEl).toBeInTheDocument();
})

test('ini button facebook', () => {
    render (<SignIn />, {wrapper: BrowserRouter});
    const buttonEl = screen.getByRole('buttonfacebook');
    expect(buttonEl).toBeInTheDocument();
})
