import React, { useEffect } from 'react'
import { Box, Grid, Typography, Card, CardContent, CardActions, CardActionArea, Button, Accordion, AccordionSummary, AccordionDetails } from '@mui/material'
import { FiUsers, FiSettings, FiCalendar } from "react-icons/fi";
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeSelectedData, fetchData, setButton } from '../Redux/actions/dataActions';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

function MetodePembayaran() {
    const data = useSelector((state) => state.data)
    const btn = useSelector((state) => state.btn.button)
    const { name, category, price, passengers, fuel, time } = data
    const { id } = useParams()
    const dispatch = useDispatch();

    useEffect(() => {
        if (id && id !== '') dispatch(fetchData(id))
        return () => {
            dispatch(setButton('Bayar'))
            dispatch(removeSelectedData())
        }
    }, [id])

    return (
        <>
            <div className='MetodePembayaran'>
                <Box className='FilterSearch' sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Box elevation={0} sx={{ display: 'flex', width: '90%', flexWrap: 'wrap', justifyContent: 'space-between' }} >
                        {Object.keys(data).length === 0 ? (
                            <div>...Loading</div>
                        ) : (
                            <Grid container sx={{ display: 'flex', justifyContent: 'space-between' }}>
                                <Grid item xs={7}>
                                    <Card sx={{ minWidth: 275 }}>
                                        <CardContent>
                                            <Box sx={{ p: 3 }}>
                                                <Typography gutterBottom variant="h6" component="div" sx={{ fontWeight: 'bold' }}>
                                                    Pilih Bank Transfer
                                                </Typography>
                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                    Kamu bisa membayar dengan transfer melalui ATM, Internet Banking atau Mobile Banking
                                                </Typography>
                                                <Box sx={{ py: 2, borderBottom: '1px solid #EEEEEE' }}>
                                                    <Button sx={{ border: '1px solid #D0D0D0', color: 'black', width: 80, mr: 3 }}>BCA</Button>
                                                    BCA Transfer
                                                </Box>
                                                <Box sx={{ py: 2, borderBottom: '1px solid #EEEEEE' }}>
                                                    <Button sx={{ border: '1px solid #D0D0D0', color: 'black', width: 80, mr: 3 }}>BNI</Button>
                                                    BNI Transfer
                                                </Box>
                                                <Box sx={{ py: 2, borderBottom: '1px solid #EEEEEE' }}>
                                                    <Button sx={{ border: '1px solid #D0D0D0', color: 'black', width: 80, mr: 3 }}>Mandiri</Button>
                                                    Mandiri Transfer
                                                </Box>
                                            </Box>
                                        </CardContent>
                                    </Card>
                                </Grid>
                                <Grid item xs={4}>
                                    <Card sx={{ maxWidth: 500, p: 2 }}>
                                        <CardActionArea>
                                            <CardContent>
                                                <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }} component="div">
                                                    {name} / {category}
                                                </Typography>
                                                <Box sx={{ display: 'flex' }}>
                                                    <FiUsers color="grey" />
                                                    <Typography gutterBottom variant="body2" color="text.secondary" sx={{ ml: 1, mr: 2 }}>
                                                        {passengers} Orang
                                                    </Typography>
                                                    <FiSettings color="grey" />
                                                    <Typography gutterBottom variant="body2" color="text.secondary" sx={{ ml: 1, mr: 2 }}>
                                                        {fuel}
                                                    </Typography>
                                                    <FiCalendar color="grey" />
                                                    <Typography variant="body2" color="text.secondary" sx={{ ml: 1 }}>
                                                        Tahun {new Date(time).getFullYear()}
                                                    </Typography>
                                                </Box>
                                            </CardContent>
                                            <CardContent>
                                                <Accordion elevation={0} sx={{ border: '0', width: "100%" }}>
                                                    <AccordionSummary
                                                        expandIcon={<ExpandMoreIcon />}
                                                        aria-controls="panel1a-content"
                                                        id="panel1a-header"
                                                        sx={{ p: 0 }}
                                                    >
                                                        <Box sx={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                                                            <Typography variant="h6" sx={{}}>Total</Typography>
                                                            <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                                                                Rp {Number(price).toLocaleString('id-ID')}
                                                            </Typography>
                                                        </Box>
                                                    </AccordionSummary>
                                                    <AccordionDetails sx={{ p: 0 }}>
                                                        <Box>
                                                            <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }} component="div">
                                                                Harga
                                                            </Typography>
                                                            <Box sx={{ display: 'flex', justifyContent: 'space-between', pl: 3 }}>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    <li>1 Mobil dengan sopir</li>
                                                                </Typography>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    Rp {Number(price).toLocaleString('id-ID')}
                                                                </Typography>
                                                            </Box>
                                                        </Box>
                                                        <Box>
                                                            <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }} component="div">
                                                                Biaya Lainnya
                                                            </Typography>
                                                            <Box sx={{ display: 'flex', justifyContent: 'space-between', pl: 3 }}>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    <li>Pajak</li>
                                                                </Typography>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16, color: '#5CB85F' }} gutterBottom variant="body2">
                                                                    Termasuk
                                                                </Typography>
                                                            </Box>
                                                            <Box sx={{ display: 'flex', justifyContent: 'space-between', pl: 3 }}>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    <li>Biaya Makan Sopir</li>
                                                                </Typography>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16, color: '#5CB85F' }} gutterBottom variant="body2">
                                                                    Termasuk
                                                                </Typography>
                                                            </Box>
                                                        </Box>
                                                        <Box sx={{ pb: 2, borderBottom: '1px solid #D0D0D0' }}>
                                                            <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }} component="div">
                                                                Belum Termasuk
                                                            </Typography>
                                                            <Box sx={{ pl: 3 }}>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    <li>Bensin</li>
                                                                </Typography>
                                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2">
                                                                    <li>Tol dan Parkir</li>
                                                                </Typography>
                                                            </Box>
                                                        </Box>
                                                        <Box sx={{ pt: 3, display: "flex", justifyContent: 'space-between' }}>
                                                            <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }}>
                                                                Total
                                                            </Typography>
                                                            <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                                                                Rp {Number(price).toLocaleString('id-ID')}
                                                            </Typography>
                                                        </Box>
                                                    </AccordionDetails>
                                                </Accordion>
                                            </CardContent>
                                        </CardActionArea>
                                        <CardActions>
                                            <Button
                                                fullWidth
                                                variant="contained"
                                                sx={{ backgroundColor: '#5CB85F', fontWeight: 'bold', py: 1, px: 4 }}
                                            >
                                                {btn}
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            </Grid>
                        )}
                    </Box>
                </Box>
            </div>
        </>
    )
}

export default MetodePembayaran