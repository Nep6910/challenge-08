import React, { useContext, useState } from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import { BsGoogle, BsFacebook, BsGithub } from "react-icons/bs";
import { Link, useNavigate } from 'react-router-dom';
import { Typography } from '@mui/material';
import axios from 'axios'
import './signin.scss'
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from '../firebase'
import {AuthContext} from '../context/AuthContext'


function SignIn() {
    const [user, setUser] = useState(null)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);

    const navigate = useNavigate()

    const {dispatch} = useContext(AuthContext)

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const res = await axios.post('http://localhost:5000/api/login', { email, password })
            setUser(res.data)
        } catch (err) {
            console.log(err);
        }
    }

    const google = () => {
        window.open("http://localhost:5000/auth/google", "_self")
    }

    const github = () => {
        window.open("http://localhost:5000/auth/github", "_self")
    }

    const handleLogin = (e) => {
        e.preventDefault()

        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                // Signed in 
                const user = userCredential.user;
                dispatch({type:'LOGIN', payload:user})
                navigate('/dashboard')
            })
            .catch((error) => {
                setError(true)
            });
    }

    return (
        <div className='SignIn'>
            <form onSubmit={handleLogin}>
                <img src="./img/logo.png" alt="" />
                <h1>Welcome, Admin BCR</h1>
                {error && <span>Masukkan username dan password yang benar. Perhatikan penggunaan huruf kapital.</span>}
                <label htmlFor="email">Email</label>
                <input type="email" placeholder='Email' onChange={e => setEmail(e.target.value)}/>
                <label htmlFor="password">Password</label>
                <input type="password" placeholder='Password' onChange={e => setPassword(e.target.value)}/>
                <button type='submit'>Sign In</button>
            </form>
            {/* <div className='form'>
                <Box
                    component="img"
                    sx={{
                        mb: "2rem",
                        width: 130
                    }}
                    alt="Gambar Tidak Ditemukan"
                    src='./img/logo.png'
                />

                <Box sx={{ mb: "2rem" }}>
                    <h1>Welcome, Admin BCR</h1>
                </Box>

                <TextField fullWidth sx={{ mb: "2rem" }}
                    id="outlined-basic"
                    label="Email" 
                    type="email"
                    variant="outlined" 
                    role="inputUser"
                    onChange={(e) => setEmail(e.target.value)}
                />

                <TextField fullWidth sx={{ mb: "2rem" }}
                    id="outlined-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    role="inputPass"
                    onChange={(e) => setPassword(e.target.value)}
                />
                <Link to='/home'>
                    <Button role="buttonsignin" sx={{ fontWeight: 'bold', backgroundColor: '#0D28A6', height: '3rem', mb: 2, "&:hover": { background: "#0D28A6" } }} fullWidth variant="contained"
                    onClick={handleSubmit}
                    >
                        Sign In
                    </Button>
                </Link>

                <Typography sx={{ textAlign: 'center', fontWeight: 'bold', mb: 2 }}>OR</Typography>

                <Box sx={{ display: 'flex', justifyContent: 'space-around' }}>
                    <Button sx={{ fontWeight: 'bold', display: 'flex', color: 'white', backgroundColor: '#df4930', height: '3rem', "&:hover": { background: "#df4930" } }} variant="contained"
                        onClick={google} role="buttongoogle">
                        <BsGoogle style={{ marginRight: '10px' }} />
                        Google
                    </Button>
                    <Button sx={{ fontWeight: 'bold', display: 'flex', color: 'white', backgroundColor: 'black', height: '3rem', "&:hover": { background: "black" } }} variant="contained"
                        onClick={github} role="buttongithub">
                        <BsGithub style={{ marginRight: '10px' }} />
                        Github
                    </Button>
                    <Button sx={{ fontWeight: 'bold', display: 'flex', color: 'white', backgroundColor: '#507cc0', height: '3rem', "&:hover": { background: "#507cc0" } }} variant="contained"
                    role="buttonfacebook">
                        <BsFacebook style={{ marginRight: '10px' }} />
                        Facebook
                    </Button>
                </Box>
            </div> */}
        </div>
    )
}

export default SignIn