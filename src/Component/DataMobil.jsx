import React, { useEffect } from 'react'
import { Box, Button } from '@mui/material'
import { Card, CardContent, CardMedia, Typography, CardActionArea, CardActions } from '@mui/material'
import { FiUsers, FiSettings, FiCalendar } from "react-icons/fi";
import { useDispatch, useSelector } from 'react-redux';
import { setButton } from '../Redux/actions/dataActions'
import { Link } from 'react-router-dom';

function DataMobil() {
    const carSearch = useSelector((state) => state.filter.searchDatas)
    // const datas = useSelector((state) => state.allDatas.datas)
    const btn = useSelector((state) => state.btn.button)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setButton('Pilih Mobil'))
    }, []);

    const renderList = carSearch.map((data) => {
        const { id, name, image, category, price, passengers, fuel, time } = data
        const rupiah = price.toLocaleString('id-ID')
        return (
            <div className='MobilCard' key={id}>
                <Card sx={{ maxWidth: 450, p: 2, mb: 5 }}>
                    <CardActionArea>
                        <CardMedia
                            sx={{ p: 2, maxHeight: 300, minHeight: 300 }}
                            component="img"
                            image={image}
                            alt="gambar mobil"
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="div">
                                {name} / {category}
                            </Typography>
                            <Typography gutterBottom variant="h5" sx={{ fontWeight: 'bold' }} component="div">
                                Rp {rupiah} / hari
                            </Typography>
                            <Typography gutterBottom variant="body2" sx={{ fontSize: 18 }}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            </Typography>
                            <Typography gutterBottom variant="body2" sx={{ fontSize: 18, display: 'flex', alignItems: 'center' }}>
                                <FiUsers size={20} style={{ marginRight: 5 }} /> {passengers} Orang
                            </Typography>
                            <Typography gutterBottom variant="body2" sx={{ fontSize: 18, display: 'flex', alignItems: 'center' }}>
                                <FiSettings size={20} style={{ marginRight: 5 }} /> {fuel}
                            </Typography>
                            <Typography variant="body2" sx={{ fontSize: 18, display: 'flex', alignItems: 'center' }}>
                                <FiCalendar size={20} style={{ marginRight: 5 }} /> Tahun {new Date(time).getFullYear()}
                            </Typography>
                        </CardContent>
                    </CardActionArea>

                    <Link to={`/home/search/detail/${id}`} style={{textDecoration:'none'}}>
                        <CardActions>
                            <Button
                                fullWidth
                                variant="contained"
                                sx={{ backgroundColor: '#5CB85F', fontWeight: 'bold', py: 1, px: 4 }}
                            >
                                {btn}
                            </Button>
                        </CardActions>
                    </Link>
                </Card>
            </div>
        );
    })

    return (
        <>
            <div className='DataMobil'>
                <Box className='FilterSearch' sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Box elevation={0} sx={{ display: 'flex', width: '90%', flexWrap: 'wrap', justifyContent: 'space-between' }} >
                        {renderList}
                    </Box>
                </Box>
            </div>
        </>
    )
}

export default DataMobil