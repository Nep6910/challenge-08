import React, { useEffect } from 'react'
import { Box, Grid, Typography, Card, CardContent, CardActions, CardActionArea, CardMedia, AccordionDetails, AccordionSummary, Accordion, Button } from '@mui/material'
import { FiUsers, FiSettings, FiCalendar } from "react-icons/fi";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Link, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { removeSelectedData, fetchData, setButton } from '../Redux/actions/dataActions';

function InfoMobil() {
    const data = useSelector((state) => state.data)
    const btn = useSelector((state) => state.btn.button)
    const { name, image, category, price, passengers, fuel, time } = data
    const { id } = useParams()
    const dispatch = useDispatch();

    useEffect(() => {
        if (id && id !== '') dispatch(fetchData(id))
        dispatch(setButton('Lanjutkan Pembayaran'))
        return () => {
            dispatch(removeSelectedData())
        }
    }, [id])

    return (
        <>
            <div className='InfoMobil'>
                <Box className='FilterSearch' sx={{ display: 'flex', justifyContent: 'center' }}>
                    <Box elevation={0} sx={{ display: 'flex', width: '90%', flexWrap: 'wrap', justifyContent: 'space-between' }} >
                        {Object.keys(data).length === 0 ? (
                            <div>...Loading</div>
                        ) : (
                            <Grid container sx={{ display: 'flex', justifyContent: 'space-between' }}>
                                <Grid item xs={7}>
                                    <Card sx={{ minWidth: 275 }}>
                                        <CardContent>
                                            <Box sx={{ p: 3 }}>
                                                <Typography gutterBottom variant="h6" component="div" sx={{ fontWeight: 'bold' }}>
                                                    Tentang Paket
                                                </Typography>
                                                <Typography sx={{ mb: 1.5, fontSize: 18 }}>
                                                    Include
                                                </Typography>
                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2" color="text.secondary">
                                                    <li>Apa saja yang termasuk dalam paket misal durasi max 12 jam</li>
                                                    <li>Sudah termasuk bensin selama 12 jam</li>
                                                    <li>Sudah termasuk Tiket Wisata</li>
                                                    <li>Sudah termasuk pajak</li>
                                                </Typography>
                                                <Typography sx={{ mb: 1.5, fontSize: 18 }}>
                                                    Exclude
                                                </Typography>
                                                <Typography sx={{ mb: 1.5, fontSize: 16 }} gutterBottom variant="body2" color="text.secondary">
                                                    <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                    <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                    <li>Tidak termasuk akomodasi penginapan</li>
                                                </Typography>
                                            </Box>
                                            <Accordion elevation={0} sx={{ border: '0' }}>
                                                <AccordionSummary
                                                    expandIcon={<ExpandMoreIcon />}
                                                    aria-controls="panel1a-content"
                                                    id="panel1a-header"
                                                >
                                                    <Typography variant="h6" component="div" sx={{ fontWeight: 'bold' }}>Refund, Reschedule, Overtime</Typography>
                                                </AccordionSummary>
                                                <AccordionDetails>
                                                    <Typography sx={{ mb: 1.5, fontSize: 16, p: 1 }} gutterBottom variant="body2" color="text.secondary">
                                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                        <li>Tidak termasuk biaya makan sopir Rp 75.000/hari</li>
                                                        <li>Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp 20.000/jam</li>
                                                        <li>Tidak termasuk akomodasi penginapan</li>
                                                    </Typography>
                                                </AccordionDetails>
                                            </Accordion>
                                        </CardContent>
                                    </Card>
                                </Grid>
                                <Grid item xs={4}>
                                    <Card sx={{ maxWidth: 500, p: 2 }}>
                                        <CardActionArea>
                                            <CardMedia
                                                sx={{ p: 2, maxHeight: 300, minHeight: 300 }}
                                                component="img"
                                                image={image}
                                                alt="gambar mobil"
                                            />
                                            <CardContent>
                                                <Typography gutterBottom variant="h6" sx={{ fontWeight: 'bold' }} component="div">
                                                    {name} / {category}
                                                </Typography>
                                                <Box sx={{ display: 'flex' }}>
                                                    <Typography gutterBottom variant="body2" color="text.secondary" sx={{ mr: 2 }}>
                                                        <FiUsers color="grey" style={{ marginRight: '0.5rem' }} />
                                                        {passengers} Orang
                                                    </Typography>
                                                    <Typography gutterBottom variant="body2" color="text.secondary" sx={{ ml: 1, mr: 2 }}>
                                                        <FiSettings color="grey" style={{ marginRight: '0.5rem' }}/>
                                                        {fuel}
                                                    </Typography>
                                                    <Typography variant="body2" color="text.secondary" sx={{ ml: 1 }}>
                                                        <FiCalendar color="grey" style={{ marginRight: '0.5rem' }}/>
                                                        Tahun {new Date(time).getFullYear()}
                                                    </Typography>
                                                </Box>
                                            </CardContent>
                                            <CardContent sx={{ display: 'flex', justifyContent: 'space-between' }}>
                                                <Typography gutterBottom variant="h6">
                                                    Total
                                                </Typography>
                                                <Typography variant="h6" sx={{ fontWeight: 'bold' }}>
                                                    Rp {Number(price).toLocaleString('id-ID')}
                                                </Typography>
                                            </CardContent>
                                        </CardActionArea>
                                        <Link to={`/home/search/detail/${id}/pembayaran`} style={{ textDecoration: 'none' }}>
                                            <CardActions>
                                                <Button
                                                    fullWidth
                                                    variant="contained"
                                                    sx={{ backgroundColor: '#5CB85F', fontWeight: 'bold', py: 1, px: 4 }}
                                                >
                                                    {btn}
                                                </Button>
                                            </CardActions>
                                        </Link>
                                    </Card>
                                </Grid>
                            </Grid>
                        )}
                    </Box>
                </Box>
                <Box sx={{ display: 'flex', justifyContent: 'center', my: 5 }}>
                    <Link to={`/home/search/detail/${id}/pembayaran`}>
                        <Button
                            fullWidth
                            variant="contained"
                            sx={{ backgroundColor: '#5CB85F', fontWeight: 'bold', py: 1, px: 4 }}
                        >
                            {btn}
                        </Button>
                    </Link>
                </Box>
            </div>
        </>
    )
}

export default InfoMobil