import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from 'firebase/firestore'
import { getStorage } from "firebase/storage";



// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration

const firebaseConfig = {

    apiKey: 'AIzaSyB8S6TBUbp8Zsx5alvynJjOgTSTmfYcBBM',

    authDomain: "binar-car-rental-3930f.firebaseapp.com",

    projectId: "binar-car-rental-3930f",

    storageBucket: "binar-car-rental-3930f.appspot.com",

    messagingSenderId: "96110605147",

    appId: "1:96110605147:web:7f11e171f822b73054e07e"

};


// Initialize Firebase

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app)
export const auth = getAuth()
export const storage = getStorage(app)
