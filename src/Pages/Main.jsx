import React from 'react'
import FilterSearch from '../GlobalComponent/FilterSearch'
import Footer from '../GlobalComponent/Footer'
import Hero from '../Component/Hero'
import Navbar from '../GlobalComponent/Navbar'
import Chart from '../Component/dashboard/chart/Chart'

const Main = ({ user }) => {
  return (
    <div className='Main'>
      <Navbar user={user} />
      <Hero />
      <FilterSearch />
      <Footer />
    </div>
  )
}

export default Main