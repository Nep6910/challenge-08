import { Box, Container, Typography } from '@mui/material'
import React from 'react'

function Error404() {
    return (
        <Container>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "100vh",
                    gap: "1rem",
                }}
            >
                <Typography variant="h3" textAlign="center">
                    Error 404
                </Typography>
                <Typography variant="h3" textAlign="center">
                    Halaman Tidak Ditemukan
                </Typography>
            </Box>
        </Container>
    )
}

export default Error404