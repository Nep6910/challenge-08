import React from 'react'
import SignIn from '../Component/SignIn'
import Wallpaper from '../Component/Wallpaper'

function SignInPages() {
    return (
        <div className='SignInPages'>
            <Wallpaper/>
            <SignIn />
        </div>
    )
}

export default SignInPages