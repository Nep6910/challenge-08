import React from 'react'
import DataMobil from '../Component/DataMobil'
import FilterSearch from '../GlobalComponent/FilterSearch'
import Footer from '../GlobalComponent/Footer'
import Navbar from '../GlobalComponent/Navbar'


function CariMobil({user}) {
    return (
        <div className='CariMobil'>
            <Navbar user={user}/>
            <div className="Pembatas"></div>
            <FilterSearch/>
            <DataMobil/>
            <Footer />
        </div>
    )
}

export default CariMobil