import "./new.scss";
import Navbar from '../../../Component/dashboard/navbar/Navbar';
import Sidebar from '../../../Component/dashboard/sidebar/Sidebar';
import Sidebar2 from "../../../Component/dashboard/sidebar2/Sidebar2";
import { FiChevronRight } from "react-icons/fi";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { addDoc, collection } from "firebase/firestore";
import { db, storage } from "../../../firebase";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";


const New = ({ inputs, title }) => {
  const [file, setFile] = useState('')
  const [data, setData] = useState({})
  const [per, setPerc] = useState(null);
  const navigate = useNavigate()

  const [alert, setAlert] = useState(false)

  var moment = require('moment');

  useEffect(() => {
    const uploadFile = () => {

      const name = new Date().getTime() + file.name

      console.log(name);
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on('state_changed',
        (snapshot) => {
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          setPerc(progress)
          switch (snapshot.state) {
            case 'paused':
              console.log('Upload is paused');
              break;
            case 'running':
              console.log('Upload is running');
              break;
            default:
              break;
          }
        },
        (error) => {
          console.log(error);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setData((prev) => ({ ...prev, img: downloadURL }))
          });
        }
      );

    }
    file && uploadFile()
  }, [file])

  const handleInput = (e) => {
    const id = e.target.id;
    const value = e.target.value;

    setData({ ...data, [id]: value })
  }

  const handleAdd = async (e) => {
    e.preventDefault();
    const delay = ms => new Promise(res => setTimeout(res, ms));
    let today = moment().format('D MMM YYYY, HH:mm');
    try {
      const res = await addDoc(collection(db, 'cars'), {
        ...data,
        createdat: today,
        updatedat: today,
      });
      setAlert(true)
      await delay(3000)
      navigate('/dashboard/cars')
    } catch (err) {
      console.log(err);
    }
  }

  return (
    <div className="new">
      <Sidebar page="Cars" />
      <div className="newContainer">
        <Navbar />
        <div className="container">
          <Sidebar2 page="CARS" title="List Car" />
          <div className="content">
            <div className="menu">
              <div className="breadcrumb">
                <p style={{ fontWeight: 'bold' }}>Cars</p>
                <FiChevronRight className="icon" />
                <Link to={'/dashboard/cars/'} style={{ textDecoration: 'none' }}>
                  <p style={{ fontWeight: 'bold', color: 'black' }}>List Car</p>
                </Link>
                <FiChevronRight className="icon" />
                <p>Add New Car</p>
              </div>
              <div className="title">
                Add New Car
              </div>
            </div>

            {alert &&
              < div className="alert">
                <span> Data Berhasil Disimpan</span>
              </div>
            }

            <form onSubmit={handleAdd} id='carForm'>
              <div className="formContainer">
                <div className="left">
                  <div className="carInput">
                    <p>Image </p>
                    <div className="inputcontainer">
                      <input type='file' id='file' onChange={(e) => setFile(e.target.files[0])} />
                    </div>
                  </div>

                  {inputs.map((input) => (
                    <div className="carInput" key={input.id}>
                      <p>{input.label}</p>
                      <div className="inputcontainer">
                        <input
                          id={input.id}
                          type={input.type}
                          placeholder={input.placeholder}
                          onChange={handleInput} />
                      </div>
                    </div>
                  ))}
                </div>

                <div className="right">
                  <img src={file ? URL.createObjectURL(file) : 'https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg'} alt="" />
                </div>
              </div>

              <div className="addButton">
                <Link to='/dashboard/cars'>
                  <button className="cancel">Cancel</button>
                </Link>
                <button disabled={per !== null && per < 100} type="submit" className="save">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div >
  );
};

export default New;
