import "./edit.scss";
import Navbar from '../../../Component/dashboard/navbar/Navbar';
import Sidebar from '../../../Component/dashboard/sidebar/Sidebar';
import Sidebar2 from "../../../Component/dashboard/sidebar2/Sidebar2";
import { FiChevronRight } from "react-icons/fi";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { collection, doc, onSnapshot, updateDoc } from "firebase/firestore";
import { db, storage } from "../../../firebase";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import { CircularProgress } from "@mui/material";


const Edit = ({ inputs }) => {
  const [file, setFile] = useState('')
  const [data, setData] = useState({})
  const [per, setPerc] = useState(null);
  const { id } = useParams()
  const [oldData, setOldData] = useState([])

  const [alert, setAlert] = useState(false)

  const navigate = useNavigate()

  var moment = require('moment');

  useEffect(() => {
    const uploadFile = () => {

      const name = new Date().getTime() + file.name

      console.log(name);
      const storageRef = ref(storage, file.name);
      const uploadTask = uploadBytesResumable(storageRef, file);

      uploadTask.on('state_changed',
        (snapshot) => {
          const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          // console.log('Upload is ' + progress + '% done');
          setPerc(progress)
          switch (snapshot.state) {
            case 'paused':
              // console.log('Upload is paused');
              break;
            case 'running':
              // console.log('Upload is running');
              break;
            default:
              break;
          }
        },
        (error) => {
          console.log(error);
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
            setData((prev) => ({ ...prev, img: downloadURL }))
          });
        }
      );

    }
    file && uploadFile()

    //LISTEN (REALTIME)
    const unsub = onSnapshot(collection(db, "cars"), (snapShot) => {
      let list = [];
      snapShot.docs.forEach(doc => {
        list.push({ id: doc.id, ...doc.data() });
      });
      setOldData(list.filter(datas => datas.id === id))
    }, (error) => {
      console.log(error);
    });

    return () => {
      unsub();
    }
  }, [file])

  const handleInput = (e) => {
    const id = e.target.id;
    const value = e.target.value;

    setData({ ...data, [id]: value })
  }

  const handleEdit = async (e) => {
    e.preventDefault();
    const delay = ms => new Promise(res => setTimeout(res, ms));
    let today = moment().format('D MMM YYYY, HH:mm');
    try {
      const res = await updateDoc(doc(db, 'cars', id), {
        ...data,
        updatedat: today,
      });
      setAlert(true)
      await delay(3000)
      navigate('/dashboard/cars')
    } catch (err) {
      console.log(err);
    }
  }

  if (oldData < 1) {
    return (
      <div className="formloading">
        <CircularProgress />
      </div>
    )
  } else {
    return (
      <div className="edit">
        <Sidebar page="Cars" />
        <div className="editContainer">
          <Navbar />
          <div className="container">
            <Sidebar2 page="CARS" title="List Car" />
            <div className="content">
              <div className="menu">
                <div className="breadcrumb">
                  <p style={{ fontWeight: 'bold' }}>Cars</p>
                  <FiChevronRight className="icon" />
                  <Link to={'/dashboard/cars/'} style={{ textDecoration: 'none' }}>
                    <p style={{ fontWeight: 'bold', color: 'black' }}>List Car</p>
                  </Link>
                  <FiChevronRight className="icon" />
                  <p>Edit Car</p>
                </div>
                <div className="title">
                  Edit Car
                </div>
              </div>

              {alert &&
                < div className="alert">
                  <span> Data Berhasil Diubah</span>
                </div>
              }

              <form onSubmit={handleEdit} id='carForm'>
                <div className="formContainer">
                  <div className="left">
                    <div className="carInput">
                      <p>Image </p>
                      <div className="inputcontainer">
                        <input type='file' id='file' onChange={(e) => setFile(e.target.files[0])} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Name</p>
                      <div className="inputcontainer">
                        <input
                          id="name"
                          type="text"
                          placeholder={oldData[0].name}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Category</p>
                      <div className="inputcontainer">
                        <input
                          id="category"
                          type="text"
                          placeholder={oldData[0].category}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Price</p>
                      <div className="inputcontainer">
                        <input
                          id="price"
                          type="number"
                          placeholder={oldData[0].price}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Passengers</p>
                      <div className="inputcontainer">
                        <input
                          id="passengers"
                          type="number"
                          placeholder={oldData[0].passengers}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Transmission</p>
                      <div className="inputcontainer">
                        <input
                          id="transmission"
                          type="text"
                          placeholder={oldData[0].transmission}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Car Year</p>
                      <div className="inputcontainer">
                        <input
                          id="caryear"
                          type="number"
                          placeholder={oldData[0].caryear}
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Start Rent</p>
                      <div className="inputcontainer">
                        <input
                          id="startrent"
                          type="date"
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Finish Rent</p>
                      <div className="inputcontainer">
                        <input
                          id="finishrent"
                          type="date"
                          onChange={handleInput} />
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Created At</p>
                      <div className="inputcontainer">
                        <p>{oldData[0].createdat}</p>
                      </div>
                    </div>

                    <div className="carInput">
                      <p>Updated At</p>
                      <div className="inputcontainer">
                        <p>{oldData[0].updatedat}</p>
                      </div>
                    </div>
                    {/* {inputs.map((input) => (
                    <div className="carInput" key={input.id}>
                      <p>{input.label}</p>
                      <div className="inputcontainer">
                        <input
                          id={input.id}
                          type={input.type}
                          placeholder={input.placeholder}
                          onChange={handleInput} />
                      </div>
                    </div>
                  ))} */}
                  </div>

                  <div className="right">
                    <img src={file ? URL.createObjectURL(file) : oldData[0].img} alt="" />
                  </div>
                </div>

                <div className="addButton">
                  <Link to='/dashboard/cars'>
                    <button className="cancel">Cancel</button>
                  </Link>
                  <button disabled={per !== null && per < 100} type="submit" className="save">Edit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  };
};

export default Edit;
