import Navbar from '../../../Component/dashboard/navbar/Navbar';
import Sidebar from '../../../Component/dashboard/sidebar/Sidebar';
import Sidebar2 from '../../../Component/dashboard/sidebar2/Sidebar2';
import Datatable from '../../../Component/dashboard/datatable/Datatable';
import Chart from '../../../Component/dashboard/chart/Chart';
import { FiChevronRight } from "react-icons/fi";
import './home.scss'
import Featured from '../../../Component/dashboard/featured/Featured';
import Widget from '../../../Component/dashboard/widget/Widget';

const Home = () => {
    return (
        <div className='home'>
            <Sidebar page="Dashboard" />
            <div className="homeContainer">
                <Navbar />
                <div className="container">
                    <Sidebar2 page="DASHBOARD" title="Dashboard" />
                    <div className="content">
                        <div className="menu">
                            <div className="breadcrumb">
                                <p style={{ fontWeight: 'bold' }}>Dashboard</p>
                                <FiChevronRight className='icon' />
                                <p>Dashboard</p>
                            </div>
                            <div className="title">
                                Dashboard
                            </div>
                            <div className="datatableTitle">
                                <p>List Car</p>
                            </div>
                        </div>
                        <Datatable />
                        <div className="widgets">
                            <Widget type='user' />
                            <Widget type='order' />
                            <Widget type='earning' />
                            <Widget type='balance' />
                        </div>
                        <div className="charts">
                            <Featured />
                            <Chart title="Last 6 Months (Revenue)" aspect={2 / 1} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;