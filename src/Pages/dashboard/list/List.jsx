import "./list.scss"
import Navbar from '../../../Component/dashboard/navbar/Navbar';
import Sidebar from '../../../Component/dashboard/sidebar/Sidebar';
import Sidebar2 from "../../../Component/dashboard/sidebar2/Sidebar2";
import { FiChevronRight, FiPlus } from "react-icons/fi";
import { Link } from "react-router-dom";
import Listcar from "../../../Component/dashboard/listcar/Listcar";

const List = () => {
  return (
    <div className="list">
      <Sidebar page="Cars"/>
      <div className="listContainer">
        <Navbar />
        <div className="container">
          <Sidebar2 page="CARS" title="List Car"/>
          <div className="content">
            <div className="menu">
              <div className="breadcrumb">
                <p style={{ fontWeight: 'bold' }}>Cars</p>
                <FiChevronRight className="icon" />
                <p>List Car</p>
              </div>
              <div className="title">
                List Car
                <Link to="/dashboard/cars/new" className="link">
                  <FiPlus className="icon"/>
                  Add New Car
                </Link>
              </div>
              <div className="allButton">
                <button className="categoryButton">All</button>
                <button className="categoryButton">Small</button>
                <button className="categoryButton">Medium</button>
                <button className="categoryButton">Large</button>
              </div>
            </div>
            <div className="carList">
              <Listcar />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default List