import React from 'react'
import FilterSearch from '../GlobalComponent/FilterSearch'
import InfoMobil from '../Component/InfoMobil'
import Footer from '../GlobalComponent/Footer'
import Navbar from '../GlobalComponent/Navbar'


function DetailMobil({user}) {
    return (
        <div className='DetailMobil'>
            <Navbar user={user}/>
            <div className="Pembatas"></div>
            <FilterSearch/>
            <InfoMobil/>
            <Footer />
        </div>
    )
}

export default DetailMobil