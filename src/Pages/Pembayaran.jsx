import React from 'react'
import MetodePembayaran from '../Component/MetodePembayaran'
import FilterSearch from '../GlobalComponent/FilterSearch'
import Footer from '../GlobalComponent/Footer'
import Navbar from '../GlobalComponent/Navbar'


function Pembayaran({user}) {
    return (
        <div className='Pembayaran'>
            <Navbar user={user}/>
            <div className="Pembatas"></div>
            <FilterSearch/>
            <MetodePembayaran/>
            <Footer />
        </div>
    )
}

export default Pembayaran