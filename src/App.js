import { Navigate, Route, Routes } from "react-router-dom";
import SignInPages from "./Pages/SignInPages";
import Error404 from './Pages/Error404'
import Main from "./Pages/Main";
import CariMobil from "./Pages/CariMobil";
import DetailMobil from "./Pages/DetailMobil";
import Pembayaran from "./Pages/Pembayaran";
import Home from './Pages/dashboard/home/Home'
import { useContext, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { fetchDatas } from "./Redux/actions/dataActions";
import List from "./Pages/dashboard/list/List";
import New from "./Pages/dashboard/new/New";
import { AuthContext } from "./context/AuthContext";
import { carInputs } from "./formSource";
import Edit from "./Pages/dashboard/edit/Edit";

function App() {
  const dispatch = useDispatch()

  const { currentUser } = useContext(AuthContext)

  const [user, setUser] = useState(false)

  const RequireAuth = ({ children }) => {
    return currentUser ? (children) : <Navigate to='/signin' />
  }

  const LogAuth = ({children}) => {
    return !currentUser ? (children) : <Navigate to='/dashboard' />
  }

  useEffect(() => {
    dispatch(fetchDatas())

    const getUser = async () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      }).then(response => {
        if (response.status === 200) return response.json();
        throw new Error('authentication has been failed!')
      }).then(resObject => {
        setUser(resObject.user)
      }).catch(err => {
        console.log(err);
      })
    };
    getUser();
  }, []);

  // console.log(currentUser);

  return (
    <Routes>
      <Route path="/">
        <Route index element={<Navigate to={"/dashboard"} />} />
        <Route path="signin" element={<LogAuth><SignInPages /></LogAuth>} />
        <Route path="home">
          <Route index element={<RequireAuth><Main /></RequireAuth>} />
          <Route path="search">
            <Route index element={<RequireAuth><CariMobil /></RequireAuth>} />
            <Route path="detail/:id">
              <Route index element={<RequireAuth><DetailMobil /></RequireAuth>} />
              <Route path="pembayaran" element={<RequireAuth><Pembayaran /></RequireAuth>} />
            </Route>
          </Route>
        </Route>
        <Route path='/dashboard'>
          <Route index element={<RequireAuth><Home /></RequireAuth>} />
          <Route path='/dashboard/cars'>
            <Route index element={<RequireAuth><List /></RequireAuth>} />
            <Route path='/dashboard/cars/new' element={<RequireAuth><New inputs={carInputs}/></RequireAuth>} />
            <Route path='/dashboard/cars/edit/:id' element={<RequireAuth><Edit inputs={carInputs}/></RequireAuth>} />
          </Route>
        </Route>
      </Route>
      <Route path="*" element={<Error404 />} />
    </Routes>
  );
}

export default App;
